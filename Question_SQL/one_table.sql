SELECT s.id, s.name, s.code FROM sections s;

-- Query with if
SELECT f.id, f.name, f.caption, f.message FROM feedbacks f
  WHERE f.menu IS NULL or f.menu = 0;

-- Query with if and order
SELECT m.id, m.name, m.descript, m.price FROM menu m
  WHERE m.section_id = 2
ORDER BY price DESC;

-- Query with group
SELECT f.id, f.name, f.caption, f.points FROM feedbacks f
  GROUP BY f.points, f.id;

