-- Query with subquery
SELECT
  o.id,
  TIME(o.date),
  concat(
      o.phone, ' :: ', o.address, ' :: ',
      (
        SELECT d.name
        FROM district d
        WHERE d.id = o.district_id
      )
  ) AS client,
  (
    SELECT s.name
    FROM status s
    WHERE s.id = o.status_id
  ) AS status
FROM orders o;

-- Queyr with LeftJoin
SELECT
  m.id,
  m.name,
  m.descript,
  m.price,
  f.path
FROM menu m
  LEFT JOIN files f
    ON f.id = (SELECT mf.file_id
               FROM menu_file mf
               WHERE m.id = mf.menu_id
               LIMIT 1);

-- Query with Exists

-- Function for Query up
DROP FUNCTION IF EXISTS empty_menu;
-- @str - string with list id elemunts menu
-- @variable - variable foreach elements menu and format output data:
--  1 - select name and check on empty element(name list : false);
--  2 - check on empty element (true : false);
--  3 - select name without check on empty element;
CREATE FUNCTION empty_menu (str VARCHAR(64), variable TINYINT(1)) RETURNS VARCHAR(196)
BEGIN
DECLARE vtime VARCHAR(8);
DECLARE item SMALLINT UNSIGNED DEFAULT 0;
DECLARE res VARCHAR(196) DEFAULT "";
SET str = REPLACE(str, ' ','');
var_loop:LOOP

  SET vtime = SUBSTRING_INDEX(str, ';', 1);
  SET str = REPLACE(str, CONCAT(vtime, ';'), '');
  SET item= SUBSTRING_INDEX(vtime, '*', '1');
  IF (variable = 1 or variable = 2) and NOT EXISTS (SELECT m.name FROM menu m WHERE m.id = item) THEN
    SET res = 'false';
    LEAVE var_loop;
  ELSEIF variable = 1 and EXISTS (SELECT m.name FROM menu m WHERE m.id = item) THEN
    SET  res = concat(res, (SELECT m.name FROM menu m WHERE m.id = item), '\n');
  ELSEIF variable = 2 and res <> 'true' THEN
    SET res = 'true';
  ELSEIF variable = 3 and EXISTS (SELECT m.name FROM menu m WHERE m.id = item) THEN
    SET  res = concat(res, (SELECT m.name FROM menu m WHERE m.id = item), '\n');
  END IF;


  IF length(str)<= 1 THEN
    LEAVE var_loop;
  END IF;
END LOOP var_loop;
  RETURN res;
END;

SELECT (empty_menu('8; 6; 5;', 1));